#!/bin/bash

source ./build-release.sh
cd ./deployment
find . -path ./.git -prune -o \( \! -path ./Procfile \) -exec rm -rf {} \; 2> /dev/null
mkdir ./server
mkdir ./lib
cp -rf ../server/* ./server
cp -rf ../lib/* ./lib
git add .
git add -u .
git commit -m "deployment"
git push heroku master