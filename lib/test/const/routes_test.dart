part of lib_test;

testRoutes() {
  group("[routes]", (){

    Routes routes;

    setUp(() {
      routes = new Routes();
    });

    test("Hello", (){
      expect(routes.hello.pattern, equals(r"/hello"));
    });
  });
}