#!/bin/bash

source ./clean.sh
cd web
echo 'Building jades...'
for file in $(find web/jade -name '*.jade')
do
  web=${file/'/jade'/''}
  out=$(echo "$web" | sed -E 's/(\/[^\/]+\.jade)$//')
  echo $out
  mkdir -p $out
  jade $file -o $out -P
done
echo 'Building styluses...'
stylus web/stylus -o web/css
echo 'Building darts...'
pub build --mode=debug web
cd ../
source ./copy-static.sh
echo 'Done!'