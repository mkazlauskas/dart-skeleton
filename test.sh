#!/bin/sh

echo "\nSERVER TESTS:"
dart server/bin/test.dart
echo "\nLIB TESTS"
dart lib/bin/test.dart
echo "\nWEB TESTS:"
karma start --single-run