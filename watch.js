var fireworm = require('fireworm');
var fw = fireworm('web/web');
var sys = require('sys');
var exec = require('child_process').exec;
var path = require('path');
function puts(error, stdout, stderr) { sys.puts(stdout) }

fw.add('**/*.jade');
fw.add('**/*.styl');

var busy = {}

function compile(filename) {
  if (busy[filename]==true) return;
  busy[filename] = true;
  setTimeout(function(){
    busy[filename]=false;
  }, 1000);

  console.log(filename + " changed!");
  if (filename.indexOf('.jade')>0) {
    out = path.dirname(filename.replace('/jade/', '/'));
    cmd = 'jade ' + filename + ' -P -o ' + out;
    exec(cmd, puts);
  } else if (filename.indexOf('.styl')>0) {
    out = path.dirname(filename.replace('/stylus/', '/css/'));
    cmd = 'stylus ' + filename + ' -o ' + out;
    exec(cmd, puts);
  }
};

fw.on('change', compile);
fw.on('add', compile);

console.log('Watching for jade or stylus changes...');