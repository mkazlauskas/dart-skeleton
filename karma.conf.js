module.exports = function(config) {
  config.set({
    basePath: 'web',
    frameworks: ['dart-unittest'],

    files: [
      {pattern: 'test/test.dart',  included: true},
      {pattern: '**/*.dart', included: false}
    ],

    exclude: [
    ],

    autoWatch: true,
    captureTimeout: 20000,
    browserNoActivityTimeout: 300000,

    plugins: [
      'karma-dart',
      'karma-chrome-launcher'
    ],

    browsers: ['Dartium']
  });
};