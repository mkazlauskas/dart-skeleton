part of server;

class ControllerHelper {
  void sendJson(HttpRequest req, obj) {
    var res = req.response;
    res.headers.contentType = ContentType.JSON;
    res.write(JSON.encode(obj));
    res.close();
  }
}