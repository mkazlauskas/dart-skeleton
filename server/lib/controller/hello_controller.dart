part of server;

class HelloController {

  ControllerHelper helper;

  void sayHello(HttpRequest req) {
    helper.sendJson(req, 'world');
  }
}