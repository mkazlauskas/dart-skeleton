part of server;

class Settings {

  int port;
  String publicDir;

  Settings() {
    var portEnv = Platform.environment['PORT'];
    if (portEnv == null) {
      port = 3000; publicDir = '../web';
    } else {
      port = int.parse(portEnv);
      publicDir = './server/web';
    }
  }

}