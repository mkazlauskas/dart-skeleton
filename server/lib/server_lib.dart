library server;

import 'dart:io';
import 'dart:convert';
import 'package:lib/lib.dart';
import 'package:route/server.dart';
import 'package:static_file_handler/static_file_handler.dart';

part 'server.dart';
part 'settings.dart';
part 'controller/controller_helper.dart';
part 'controller/hello_controller.dart';