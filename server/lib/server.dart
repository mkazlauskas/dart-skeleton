part of server;

class Server {

  Router router;
  Routes routes;
  HelloController helloCtrl;
  StaticFileHandler _fileHandler;

  StaticFileHandler get fileHandler => _fileHandler;
                    set fileHandler(newHandler) {
                      newHandler.addMIMETypes(<String, String> {
                            ".js": "application/javascript",
                            ".css": "text/css",
                            ".html": "text/html"
                          });
                      _fileHandler = newHandler;
                    }

  void configRoutes() {
    router
      ..serve(routes.hello).listen(helloCtrl.sayHello)
      ..defaultStream.listen(handleStatic);
  }

  void handleStatic(HttpRequest req) {
    fileHandler.handleRequest(req);
  }
}