part of server_lib_test;

testServer() {
  group("[Server]", () {

    HelloController helloCtrl;
    Server server;

    setUp((){
      helloCtrl = new HelloController();
      server = new Server()
        ..helloCtrl = helloCtrl;
    });

    group("configRoutes()", (){

      Routes routes;
      MockRouter router;
      MockStream<HttpRequest> helloStream;
      MockStream<HttpRequest> defaultStream;

      setUp((){
        routes = new Routes();
        router = new MockRouter();
        server
          ..router = router
          ..routes = routes;
        helloStream = new MockStream<HttpRequest>();
        defaultStream = new MockStream<HttpRequest>();
        router.when(callsTo('serve', routes.hello)).alwaysReturn(helloStream);
        router.when(callsTo('get defaultStream')).alwaysReturn(defaultStream);
        server.configRoutes();
      });

      test("serves /hello", (){
        router.getLogs(callsTo('serve', routes.hello)).verify(happenedOnce);
        helloStream.getLogs(callsTo('listen', (fn)=>fn==helloCtrl.sayHello)).verify(happenedOnce);
      });

      test("serves static files", (){
        router.getLogs(callsTo('get defaultStream')).verify(happenedOnce);
        defaultStream.getLogs(callsTo('listen', (fn)=>fn==server.handleStatic)).verify(happenedOnce);
      });

    });

    group('set fileHandler', (){

      MockStaticFileHandler fileHandler;

      setUp((){
        fileHandler = new MockStaticFileHandler();
        server.fileHandler = fileHandler;
      });

      test('adds mime types', (){
        var logs = fileHandler.getLogs(callsTo('addMIMETypes'));
        logs.verify(happenedOnce);
        LogEntry log = logs.first;
        Map<String, String> types = log.args[0];
        expect(types['.js'], equals('application/javascript'));
        expect(types['.css'], equals('text/css'));
        expect(types['.html'], equals('text/html'));
      });

    });

    group('handleStatic()', (){

      MockStaticFileHandler fileHandler;
      MockHttpRequest req;

      setUp((){
        fileHandler = new MockStaticFileHandler();
        req = new MockHttpRequest();
        server.fileHandler = fileHandler;
        server.handleStatic(req);
      });

      test('uses file handler', (){
        fileHandler.getLogs(callsTo('handleRequest', req)).verify(happenedOnce);
      });

    });

  });
}
