part of server_lib_test;

testControllerHelper() {
  group("[ControllerHelper]", () {

    ControllerHelper ctrl;
    MockHttpRequest req;
    MockHttpResponse res;
    MockHttpHeaders headers;

    setUp((){
      ctrl = new ControllerHelper();
      res = new MockHttpResponse();
      req = new MockHttpRequest();
      headers = new MockHttpHeaders();
      req.when(callsTo('get response')).alwaysReturn(res);
      res.when(callsTo('get headers')).alwaysReturn(headers);
    });

    group("sendJson()", () {
      setUp(() => ctrl.sendJson(req, 'hello'));

      test("writes json, sets content type and closes req", () {
        headers.getLogs(callsTo('set contentType', ContentType.JSON)).verify(happenedOnce);
        res.getLogs(callsTo('write', '"hello"')).verify(happenedOnce);
        res.getLogs(callsTo('close')).verify(happenedOnce);
      });
    });

  });
}
