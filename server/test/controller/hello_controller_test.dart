part of server_lib_test;

testHelloController() {
  group("[HelloController]", () {

    HelloController ctrl;
    MockHttpRequest req;
    MockControllerHelper helper;

    setUp(() {
      helper = new MockControllerHelper();
      ctrl = new HelloController()
        ..helper = helper;
      req = new MockHttpRequest();
    });

    group("sayHello(req)", () {
      setUp(() => ctrl.sayHello(req));

      test("writes 'world' as json", () {
        helper.getLogs(callsTo('sendJson', req, 'world')).verify(happenedOnce);
      });
    });

  });
}
