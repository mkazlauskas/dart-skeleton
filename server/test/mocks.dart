part of server_lib_test;

class MockRouter extends Mock implements Router {}
class MockStream<T> extends Mock implements Stream<T> {}
class MockHttpRequest extends Mock implements HttpRequest {}
class MockHttpResponse extends Mock implements HttpResponse {}
class MockStaticFileHandler extends Mock implements StaticFileHandler {}
class MockControllerHelper extends Mock implements ControllerHelper {}
class MockHttpHeaders extends Mock implements HttpHeaders {}