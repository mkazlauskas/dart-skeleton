library server_lib_test;

import 'dart:io';
import 'dart:async';
import 'package:lib/lib.dart';
import 'package:server/server_lib.dart';
import 'package:unittest/unittest.dart';
import 'package:mock/mock.dart';
import 'package:route/server.dart';
import 'package:static_file_handler/static_file_handler.dart';

part 'server_test.dart';
part 'mocks.dart';
part 'controller/hello_controller_test.dart';
part 'controller/controller_helper_test.dart';

testServerLib() {
  testServer();
  testControllerHelper();
  testHelloController();
}