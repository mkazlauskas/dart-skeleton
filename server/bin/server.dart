import 'dart:io';
import 'package:route/server.dart';
import 'package:server/server_lib.dart';
import 'package:lib/lib.dart';
import 'package:static_file_handler/static_file_handler.dart';

void main() {
  var settings = new Settings();
  HttpServer.bind(InternetAddress.ANY_IP_V4, settings.port).then((httpServer){
    var router = new Router(httpServer);
    var server = new Server()
      ..router = router
      ..routes = new Routes()
      ..helloCtrl = (new HelloController()
        ..helper = new ControllerHelper())
      ..fileHandler = new StaticFileHandler.serveFolder(settings.publicDir);
    server.configRoutes();
  });
}