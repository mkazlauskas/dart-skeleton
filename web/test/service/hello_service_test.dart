part of service_lib_test;

testHelloService(){

  group('getHello()', (){

    MockHttp http;
    MockEnv env;
    MockRoutes routes;
    HelloService service;

    setUp((){
      env = new MockEnv();
      routes = new MockRoutes();
      http = new MockHttp();
    });

    group('in production mode', (){

      const String url = '/hello';
      const String response = 'world';

      setUp((){
        routes.when(callsTo('get hello')).alwaysReturn(new UrlPattern(url));
        http.when(callsTo('get', url)).alwaysReturn(
            new Future<HttpResponse>.value(new HttpResponse(200, '"$response"')));
        env.when(callsTo('get inProduction')).alwaysReturn(true);
        service = new HelloService(env, http, routes);
      });

      test('calls backend hello', (){
        expect(service.getHello(), completion(response));
      });

    });

    group('in debug mode', (){

      setUp((){
        env.when(callsTo('get inProduction')).alwaysReturn(false);
        service = new HelloService(env, http, routes);
      });

      test('doesnt call backend hello', (){
        expect(service.getHello(), completion(isNotNull));
        routes.verifyZeroInteractions();
        http.verifyZeroInteractions();
      });

    });

  });

}