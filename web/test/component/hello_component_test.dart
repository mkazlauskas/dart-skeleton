part of component_lib_test;

testHelloComponent(){
  group('[HelloComponent]', (){

    const helloResult = 'world';
    MockHelloService helloService;
    HelloComponent cmp;

    setUp((){
      helloService = new MockHelloService();
      helloService.when(callsTo('getHello')).alwaysReturn(new Future.value(helloResult));
      cmp = new HelloComponent(helloService);
    });

    test('sayHello()', (){
      cmp.sayHello().then(expectAsync((_)=>expect(cmp.who, equals(helloResult))));
    });

  });
}