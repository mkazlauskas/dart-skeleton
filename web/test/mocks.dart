library mocks;

import 'package:web/service_lib.dart';
import 'package:mock/mock.dart';
import 'package:angular/angular.dart';
import 'package:lib/lib.dart';
import 'package:route/url_pattern.dart';

class MockHelloService extends Mock implements HelloService {}
class MockHttp extends Mock implements Http {}
class MockEnv extends Mock implements Env {}
class MockRoutes extends Mock implements Routes {}