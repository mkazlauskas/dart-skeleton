library service_lib_test;
import 'package:unittest/unittest.dart';
import 'package:web/service_lib.dart';
import 'mocks.dart';
import 'package:mock/mock.dart';
import 'package:angular/angular.dart';
import 'package:route/url_pattern.dart';
import 'dart:async';

part 'service/hello_service_test.dart';

testServiceLib() {
  testHelloService();
}