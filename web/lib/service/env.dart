part of service;

class Env {

  bool inProduction;

  Env() {
    inProduction = const String.fromEnvironment('IS_PRODUCTION') == 'true';
  }

}