part of service;

typedef Future<String> GetHello();

class HelloService {

  GetHello getHello;
  Http http;
  Routes routes;

  HelloService(Env env, Http this.http, Routes this.routes) {
    getHello = env.inProduction ? _getHello : _getHelloFake;
  }

  Future<String> _getHello() {
    return new Future<String>(()=>
        http.get(routes.hello.pattern).then((_)=>JSON.decode(_.data)));
  }

  Future<String> _getHelloFake() {
    return new Future.value('fake world');
  }

}