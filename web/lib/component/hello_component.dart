part of component;

@Component(
    selector: 'hello',
    publishAs: 'cmp',
    templateUrl: 'views/component/hello.html',
    useShadowDom: false)
class HelloComponent {

  String who;

  HelloService hello;

  HelloComponent(HelloService this.hello);

  Future sayHello() => hello.getHello().then((_) => who = _);

}
