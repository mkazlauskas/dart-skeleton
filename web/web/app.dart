library web;

import 'package:angular/application_factory.dart';
import 'package:lib/lib.dart';
import 'package:angular/angular.dart';
import 'package:web/component_lib.dart';
import 'package:web/service_lib.dart';

// This annotation allows Dart to shake away any classes
// not used from Dart code nor listed in another @MirrorsUsed.
//
// If you create classes that are referenced from the Angular
// expressions, you must include a library target in @MirrorsUsed.
@MirrorsUsed(targets: const[
  App, Env, HelloComponent, HelloService, Routes], override: '*')
import 'dart:mirrors';

class App extends Module {
  App() {
    bind(HelloComponent);
    bind(Env);
    bind(Routes);
    bind(HelloService);
    bind(RouteInitializerFn, toValue: (Router router, RouteViewFactory views) {
      views.configure({
        'root': ngRoute(
          path: '/',
          view: 'views/root.html')
      });
    });
  }
}

void main() {
  applicationFactory()
    ..addModule(new App())
    ..run();
}