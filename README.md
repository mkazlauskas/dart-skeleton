# Dart Application Skeleton

For applications that uses Dart for both client and server.

## Prerequisites

This README assumes that you're running Linux OS. Most of the instructions should also work on Mac. On Windows you will need to do some things differently.

* [git][3]
* [Dart Editor bundle][2] (Dart + Dartium + editor) [*learn dart*][4]
* [Node.js][7]

## Set Up

1. Install prerequisites and add dart executables to PATH. This can be done using symlinks to */bin* directory *(ln -s src dest)* or by using *export*.
3. Clone the project
        
        git clone the repository
        cd <dir>

4. Install required npm packages:

        npm install
        sudo npm install -g karma-cli

5. Set Dartium environment variable: `export DARTIUM_BIN=/usr/local/dart/chromium/chrome` (your path may be different)
6. Run tests to confirm everything is ok

        chmod +x *.sh
        ./test.sh

## Structure

Project is divided into 3 subprojects:

1. **server** - dart backend server
2. **web** - static frontend files (dart, html & css)
3. **lib** - dart libraries shared between *server* and *web*

Structure of each subproject follows [Pub Package Layout Conventions][1].

## Workflow

Subprojects are developed independently. 

* When developing on **server**, most of the time unit tests should be enough to test new functionality. *Run* in editor should be set to execute `bin/test.dart`. To test your API in browser *Run* `bin/server.dart`.
* When developing on **lib**, *Run* in editor should be set to execute `bin/test.dart`.
* When developing on **web**, *Run* in editor should be set to run development server in *Dartium* (configured as `web/index.html`). Use `node watch.js` command to automatically recompile css and html. Dartium will automatically reload the page when css is recompiled (doesn't work for templates). Use `karma start` to run unit tests and watch for dart file changes.

Switching between projects in *Dart Editor* is done by opening a *pubspec.yaml*.

## Commands

* Build *web* subproject, **Jade** and **Stylus** files in *debug* mode and copy static files under *server/web*: `./build.sh`
* Build *web* subproject, **Jade** and **Stylus** in *release* mode (with minification) and copy static files under *server/web*: `./build-release.sh`.
* Watch for **Jade** and **Stylus** changes to recompile on file save: `node watch.js`
* Run tests of all 3 subprojects: `./test.sh`

## Glue

Development server in **web** will not be able to query the server so you will need to have 2 different implementations of data services: 

* a real one, that queries the server in production mode.
* a fake (design-time) one, that uses in-memory, hardcoded data.

The flag to see which mode you're in is set in *Env* service. See example at `web/lib/service/hello_service.dart`.

To test the whole app in browser you will have to run `build` command, start **server's** `bin/server.dart` and open in browser at *http://localhost:3000*. Production variable will be set on dart2js build, so if you're working on **web** development server in Dartium you will use fake services, otherwise the real ones.

## Deployment

Example for deployment to Heroku

### Set up

        mkdir deployment
        cd deployment
        echo "web: ./dart-sdk/bin/dart server/bin/server.dart" > Procfile
        git init
        git add .
        git commit -m "init"
        heroku create APP_NAME -s cedar
        heroku config:set BUILDPACK_URL=https://github.com/selkhateeb/heroku-buildpack-dart.git
        heroku config:set DART_SDK_URL=https://github.com/selkhateeb/heroku-vagrant-dart-build/releases/download/latest/dart-sdk.tar

### Deploy
  
  `./deploy.sh`

## TODO

Add some basic database support

[1]: https://www.dartlang.org/tools/pub/package-layout.html
[2]: https://www.dartlang.org/tools/editor/
[3]: http://www.git-scm.com/
[4]: https://www.dartlang.org/docs/dart-up-and-running/
[5]: http://jade-lang.com/
[6]: http://learnboost.github.io/stylus/
[7]: http://nodejs.org